/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.boalis.mini.di;

import java.lang.reflect.Method;

/**
 *
 * @author santi
 */
public class BasicMethodEventHandler extends MethodEventHandler{
    
    @Override
    public Object[] beforeInvoke(Method m, Object[] args) throws Throwable {
        if (args==null){
            return args;
        }
        System.out.println(m.getName());
        for (Object o: args){
            System.out.println(o);
            
        }        
        return args;
    }
        

    @Override
    public Object afterInvoke(Method m, Object[] args, Object result) throws Throwable {
        System.out.println(result);
        return result;
    }
    
}
