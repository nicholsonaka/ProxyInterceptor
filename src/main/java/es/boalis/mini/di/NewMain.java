/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.boalis.mini.di;

/**
 *
 * @author santi
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InstantiationException, IllegalAccessException {
       
        Container container = Container.getInstance();
        container.register("1", SayHelloImpl.class,new BasicMethodEventHandler());        
        SayHello hello = (SayHello)container.getBean("1");
        System.out.println(hello.to("santiago"));
        
    }
    
}
