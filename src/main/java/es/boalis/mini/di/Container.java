/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.boalis.mini.di;

import java.util.HashMap;
import java.util.Properties;

/**
 *
 * @author santi
 */
public class Container {
    private static Container container;
    
    private HashMap<String,Object> map = new HashMap<String,Object>();
    
    private Container(){        
    }
    
    public static Container getInstance(){
        if(container==null){
            container = new Container();
        }
        return container;
    }
    
    public void configure(Properties p){
        //
    }
    
    public void clean(){
        map.clear();
    }
    
    public void register(String key,Class impl,MethodEventHandler methodInterceptorHandler) throws InstantiationException, IllegalAccessException{
        Object o = ProxyInvoker.newInstance(impl.newInstance(),methodInterceptorHandler);
        map.put(key, o);
    }
    
    public Object getBean(String name) throws InstantiationException, IllegalAccessException{
        return map.get(name);        
    }
    
    
    
}
