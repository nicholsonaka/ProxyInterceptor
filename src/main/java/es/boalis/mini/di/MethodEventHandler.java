/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.boalis.mini.di;

import java.lang.reflect.Method;

/**
 *
 * @author santi
 */
public abstract class MethodEventHandler {
    public abstract Object[] beforeInvoke(Method m, Object[] args) throws Throwable;
    public abstract Object afterInvoke(Method m, Object[] args, Object result) throws Throwable;
    
}
