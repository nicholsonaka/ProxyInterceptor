/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.boalis.mini.di;

/**
 *
 * @author santi
 */
public class SayHelloImpl implements SayHello{

    @Override
    public void doIt() {
        System.out.println("sayHello");
    }

    @Override
    public String to(String name) {
        return "say Hello World to "+name;
    }
    
}
