/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.boalis.mini.di;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 *
 * @author santi
 */
public class ProxyInvoker implements InvocationHandler{
    private Object proxy = null;
    private MethodEventHandler eventHandler;
    private ProxyInvoker(Object p,MethodEventHandler interceptor) {
        proxy = p;
        eventHandler = interceptor;                
    }

    
    public static Object newInstance(Object o,MethodEventHandler interceptor){
         return java.lang.reflect.Proxy.newProxyInstance(o.getClass().getClassLoader(),
                 o.getClass().getInterfaces(),
                 new ProxyInvoker(o,interceptor));
    }
    
    @Override
    public Object invoke(Object M, Method method, Object[] args) throws Throwable {
        Object result;
        try {
            if (eventHandler != null) {
                args = eventHandler.beforeInvoke(method, args);
                result = method.invoke(proxy, args);
                result = eventHandler.afterInvoke(method, args, result);

            } else {
                result = method.invoke(proxy, args);
            }
            return result;
        } catch (InvocationTargetException e) {
            throw e.getTargetException();
        } catch (IllegalAccessException | IllegalArgumentException e) {
            throw new RuntimeException("unexpected invocation exception: " +
                                       e.getMessage());
        } 
        
    }
    
}
