/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.boalis.mini.di;

/**
 *
 * @author santi
 */
public interface SayHello {
    public void doIt();
    public String to(String name);
}
